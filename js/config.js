tailwind.config = {
    theme: {
      extend:{
        fontFamily:{
          'hallo': ['hallo'],
        },
        backgroundImage: {
          'hero': "url('../img/background.gif')",
        },
        boxShadow: {
          'sha': '0 4px 30px 20px rgb(212, 0, 212)',
        },

      }
    },
  }