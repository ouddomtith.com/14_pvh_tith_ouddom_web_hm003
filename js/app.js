const x = document.getElementById("startTime");
const y = document.getElementById("stopTime");
const showDate = document.getElementById("h2");
const startBtn = document.getElementById("click");
const minute = document.getElementById("minute");
const real = document.getElementById("real");
let startValue = startBtn.value;
// function timer
const store = setInterval(function(){dateTime();},1000);
function dateTime(){
    const d = new Date(); 
    console.log(d);
    const date = d.toLocaleDateString("en-US", {
            weekday: "short",
            year: "numeric",
            month: "short",
            day: "numeric",
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit",
    });
    showDate.innerHTML = date;
}
//function starttime
let startTime;
const myTimer = () => {
    startTime = new Date();
    const con = startTime.toLocaleTimeString("en-US",{
        hour:"2-digit",minute:"2-digit",
    });
    x.innerHTML = con;
}
//function stoptime
const stopTimer = () => {
  let stopDate = new Date();
  let date = stopDate.toLocaleTimeString("en-US", {
    hour: "2-digit",
    minute: "2-digit",
  });
  let totalTime = (stopDate - startTime) / 60000;
  totalMoney(totalTime);
  minute.innerHTML = Math.trunc(totalTime);
  y.innerHTML = date;
}
//function totalTime
const totalMoney = (totalTime) => {
  if (totalTime <= 15) {
    real.innerHTML = 500;
  } else if (totalTime <= 30) {
    real.innerHTML = 1000;
  } else if (totalTime <= 60) {
    real.innerHTML = 1500;
  } else if (totalTime > 60) {
    let divideTime = totalTime % 60;
    let time = Math.trunc(totalTime / 60);
    if (divideTime > 0) {
      if (divideTime > 0 && divideTime <= 15) {
        real.innerHTML = time * 1500 + 500;
      } else if (divideTime <= 30) {
        real.innerHTML = time * 1500 + 1000;
      } else if (divideTime <= 60) {
        real.innerHTML = time * 1500 + 1500;
      }
    } else {
      real.innerHTML = overPriceInt * 1500;
    }
  }
}
//function cleartime
const clearTime = () => {
    y.innerHTML = "00:00";
    x.innerHTML = "00:00";
    real.innerHTML="0";
    minute.innerHTML="0";
}
//function button
function increment(){
    switch(startValue){
        case "Start":
            startValue = "Stop";
            startBtn.innerHTML = "Stop <ion-icon class='ml-1 text-4xl' name='stop-circle-outline'></ion-icon>";
            myTimer();
            break;
        case "Stop": 
            startValue = "Clear";
            startBtn.innerHTML = "Clear <ion-icon class='ml-1 text-4xl' name='trash-outline'></ion-icon>";
            stopTimer();
            break;
        case "Clear":
            startValue = "Start"
            startBtn.innerHTML = "Start <ion-icon class='ml-1 text-4xl' name='alarm-outline'></ion-icon>";
            clearTime();
            break;
    }
}



